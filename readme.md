# Solución de prueba técnica

La solución a los 3 ejercicios está dentro de la carpeta `src`

Para la ejecución del primer ejercicio:

* Lanzar un `composer install`.
* Ejecutar  `php src/Ej1/UI/Ej1.php`
> Ejemplo de resultado:
```
Array
(
    [0] => Array
        (
            [id] => 1
            [name] => Shoes
            [parent] => 
            [children] => Array
                (
                    [0] => Array
                        (
                            [id] => 4
                            [name] => Sandals
                            [parent] => 1
                            [children] => Array
                                (
                                    [0] => Array
                                        (
                                            [id] => 5
                                            [name] => Water sandals
                                            [parent] => 4
                                        )

                                )

                        )

                )

        )

    [1] => Array
        (
            [id] => 2
            [name] => Clothes
            [parent] => 
            [children] => Array
                (
                    [0] => Array
                        (
                            [id] => 3
                            [name] => Dresses
                            [parent] => 2
                            [children] => Array
                                (
                                    [0] => Array
                                        (
                                            [id] => 6
                                            [name] => Party dresses
                                            [parent] => 3
                                        )

                                )

                        )

                    [1] => Array
                        (
                            [id] => 7
                            [name] => Trousers
                            [parent] => 2
                        )

                )

        )

)
```

Los demás ejercicios están resueltos pregunta por pregunta en ficheros con formato Markdown o SQL.