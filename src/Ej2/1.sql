# Realiza una consulta en SQL que muestre los últimos 50 productos de las usuarias que
# sigue Beatriz. Dichos productos han de estar creados la última semana como mucho

SELECT
    p.*
FROM
    `product` p
        INNER JOIN
    `follow` f ON p.user_id = f.user_followed_id
        INNER JOIN
    `user` u ON u.id = f.user_follower_id
WHERE
        u.id = 3
  AND p.date > NOW() - INTERVAL 7 DAY
ORDER BY p.date DESC
    LIMIT 0 , 50;