<?php declare(strict_types=1);

namespace GoTrendier\Ej1\Infrastructure\Repository;

use GoTrendier\Ej1\Domain\Category\CategoryRepositoryInterface;

class CategoryRepository implements CategoryRepositoryInterface
{

    public function getCategories(): array
    {
        return [
            ['id' => 1, 'name' => 'Shoes', 'parent' => null],
            ['id' => 2, 'name' => 'Clothes', 'parent' => null],
            ['id' => 3, 'name' => 'Dresses', 'parent' => 2],
            ['id' => 4, 'name' => 'Sandals', 'parent' => 1],
            ['id' => 5, 'name' => 'Water sandals', 'parent' => 4],
            ['id' => 6, 'name' => 'Party dresses', 'parent' => 3],
            ['id' => 7, 'name' => 'Trousers', 'parent' => 2],
        ];
    }
}