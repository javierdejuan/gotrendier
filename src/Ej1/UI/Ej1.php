<?php declare(strict_types=1);

require __DIR__ . '/../../../vendor/autoload.php';

use GoTrendier\Ej1\Application\UseCase\Category\Handler\Ej1Handler;
use GoTrendier\Ej1\Application\UseCase\Category\Service\SortCategoriesAsTreeService;
use GoTrendier\Ej1\Infrastructure\Repository\CategoryRepository;

final class Ej1
{
    public static function main(): void
    {
        $categoryRepository = new CategoryRepository();
        $sortCategoriesService = new SortCategoriesAsTreeService();

        $ej1 = new Ej1Handler($categoryRepository, $sortCategoriesService);
        $ej1->resolve();
    }
}

Ej1::main();
