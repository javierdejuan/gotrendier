<?php declare(strict_types=1);

namespace GoTrendier\Ej1\Domain\Category;

interface CategoryRepositoryInterface
{
    public function getCategories(): array;
}