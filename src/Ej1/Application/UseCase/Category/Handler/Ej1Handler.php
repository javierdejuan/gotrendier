<?php declare(strict_types=1);

namespace GoTrendier\Ej1\Application\UseCase\Category\Handler;

use GoTrendier\Ej1\Application\UseCase\Category\Service\SortCategoriesInterface;
use GoTrendier\Ej1\Infrastructure\Repository\CategoryRepository;

final class Ej1Handler
{
    private SortCategoriesInterface $sortCategoriesService;
    private CategoryRepository $categoryRepository;

    public function __construct(
        CategoryRepository          $categoryRepository,
        SortCategoriesInterface $sortCategoriesService
    )
    {
        $this->categoryRepository = $categoryRepository;
        $this->sortCategoriesService = $sortCategoriesService;
    }

    public function resolve(): void
    {
        $categories = $this->categoryRepository->getCategories();
        $categoriesTree = $this->sortCategoriesService->sort($categories);

        $this->printResult($categoriesTree);
    }

    private function printResult(array $categories): void
    {
        print("<pre>" . print_r($categories, true) . "</pre>");
    }
}
