<?php declare(strict_types=1);

namespace GoTrendier\Ej1\Application\UseCase\Category\Service;

final class SortCategoriesAsTreeService implements SortCategoriesInterface
{
    public function sort(array $categories): array
    {
        return $this->getCategoriesTree($categories, null);
    }

    private function getCategoriesTree(array $categories, ?int $categoryParentId): array
    {
        $categoriesTree = array();

        foreach ($categories as $category) {
            if ($category['parent'] !== $categoryParentId) {
                continue;
            }

            $categoryChildren = $this->getCategoriesTree($categories, $category['id']);

            if ($categoryChildren) {
                $category['children'] = $categoryChildren;
            }

            $categoriesTree[] = $category;
        }

        return $categoriesTree;
    }
}