<?php declare(strict_types=1);

namespace GoTrendier\Ej1\Application\UseCase\Category\Service;

interface SortCategoriesInterface
{
    public function sort(array $categories): array;
}